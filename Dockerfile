# définit l'image à utiliser
FROM node:10
#Définit le chemin 
WORKDIR /app
# Move l'image 
COPY . .
#RUN est la commande exécutée pendant le Docker Build
RUN npm install
#CMD est le process par défaut exécuté par Docker Run. Syntaxe : un mot par "..."
CMD ["npm","start"]
