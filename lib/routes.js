exports.order = function order(req, res, next) {
  // TODO implement from here
  try {
    console.log(req.body);

    let priceHT = calculatePriceHT(req.body.prices, req.body.quantities);
    let priceTTC = calculatePriceTTC(priceHT, req.body.country);
    if (priceTTC ==-1){
      res.status(400).end()
    }
    let reduction = calculateReduction(priceTTC, req.body.reduction, priceTTC);
    var reducedPrice = priceTTC - priceTTC*reduction;
  
    if(checkRequest(req) == -1){
      console.log('Bad Request *************');
      
      res.status(400).end()
    }
    else{
      res.json({'total' : reducedPrice });
    }
  }
  catch{
    res.status(400).end()
  }

};

function checkRequest(req){

  // if(req.body.length == 0){
  //   return -1;
  // }
  if (req.body.hasOwnProperty("prices") ==false || req.body.hasOwnProperty("quantities") ==false || req.body.hasOwnProperty("country") ==false || req.body.hasOwnProperty("reduction") ==false){
    return (-1);
  }

  else if (req.body.prices.length != req.body.quantities.length){
    return (-1);
  }

  else {
    return (0);
  }

}

function calculatePriceHT(prices, quantities){
  if (prices.length == quantities.length){
    let sum = 0;
    for (var i=0; i<prices.length; i++){
      sum += prices[i]*quantities[i];
    }
    return sum;
  }
  else { 
    return -1;
  }
}

function calculateReduction (country, reductionPolicy, priceTTC){
  
  let reduction = 0;
  
  switch (reductionPolicy) {
    case 'PAY THE PRICE':
      reduction = 0
      break;
    case 'HALF PRICE':
      reduction = 0.5
      break;
    case 'STANDARD':
      if (priceTTC >= 50000) {
        reduction = 0.15;
      }
      else if (priceTTC >= 10000) {
        reduction = 0.10;
      }
      else if (priceTTC >= 7000) {
        reduction = 0.07;
      }
      else if (priceTTC >= 5000) {
        reduction = 0.05;
      }
      else if (priceTTC >= 1000) {
        reduction = 0.03;
      }
      else {
        reduction = 0
      }
      break;
  }
  return reduction;
}

function calculatePriceTTC (priceHT, country){
  var taxes = {
    'DE': 0.2,
    'UK': 0.21,
    'FR': 0.2,
    'IT': 0.25,
    'ES': 0.19,
    'PL': 0.21,
    'RO': 0.2,
    'NL': 0.2,
    'BE': 0.24,
    'EL': 0.2,
    'CZ': 0.19,
    'PT': 0.23,
    'HU': 0.27,
    'SE': 0.23,
    'AT': 0.22,
    'BG': 0.21,
    'DK': 0.21,
    'FI': 0.17,
    'SK': 0.18,
    'IE': 0.21,
    'HR': 0.23,
    'LT': 0.23,
    'SI': 0.24,
    'LV': 0.2,
    'EE': 0.22,
    'CY': 0.21,
    'LU': 0.25,
    'MT': 0.2
  };

  if(country in taxes){
    let totalPrice = priceHT * (Number(taxes[country] + 1));
    return totalPrice;
  }
  else{
    return -1;
  }
}



exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};
